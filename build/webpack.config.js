const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const baseConfig = require('./webpack.base.config');
const DevServerConfig = require('./dev-server.config');

const publicPath = process.env.PUBLIC_PATH || process.env.npm_package_config_default_public_path;
const distPath = path.resolve(__dirname, '../dist');

const analyzerPlugins = process.env.ANALYZE ? [new BundleAnalyzerPlugin({
	analyzerMode: 'static',
	generateStatsFile: true
})] : [];

module.exports = merge(baseConfig, {
	entry: {
		app: './src/main.js'
	},
	output: {
		path: distPath,
		publicPath,
		filename: '[name]-[hash].js',
		chunkFilename: '[name].bundle-[hash].js',
	},
	optimization: {
		splitChunks: { chunks: 'all' }
	},
	plugins: [
		new CleanWebpackPlugin(),
		...analyzerPlugins,
		new webpack.HotModuleReplacementPlugin(),
		new webpack.DefinePlugin({
			'process.env.PUBLIC_PATH': JSON.stringify(publicPath)
		}),
		new CopyWebpackPlugin([{ from: './static' }]),
	],
	performance: {
		maxEntrypointSize: 512000,
		maxAssetSize: 512000
	},
	devServer: DevServerConfig({ publicPath })
});
