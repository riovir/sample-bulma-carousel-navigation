const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');

const lintLoader = { loader: 'eslint-loader', options: { failOnError: true } };
const cssLoaders = [CssExtractPlugin.loader, withSourceMap('css-loader'), withSourceMap('postcss-loader')];
const babelInclude = projectPaths('src', 'test');

module.exports = {
	resolve: {
		alias: {
			'src': path.resolve(__dirname, '../src'),
			'test': path.resolve(__dirname, '../test')
		},
		extensions: ['*', '.js', '.vue', '.json'],
		mainFiles: ['index', 'index.vue']
	},
	module: {
		rules: [
			{ test: /\.vue$/, ...lintLoader, enforce: 'pre', exclude: /node_modules/ },
			{ test: /\.vue$/, loader: 'vue-loader' },
			{ test: /\.js$/, use: ['babel-loader', lintLoader], include: babelInclude },
			{ test: /\.css$/, use: cssLoaders },
			{ test: /\.(scss|sass)$/, use: [...cssLoaders, 'resolve-url-loader', withSourceMap('sass-loader')] },
			{
				test: /\.woff(2)?$/,
				loader: 'url-loader',
				options: { limit: 10000, mimetype: 'application/font-woff', name: 'fonts/[name]-[hash].[ext]' }
			},
			{ test: /\.(ttf|eot)$/, loader: 'file-loader', options: { name: 'fonts/[name]-[hash].[ext]' } }
		]
	},
	plugins: [
		new VueLoaderPlugin(),
		new CssExtractPlugin({ filename: '[name]-[hash].css', chunkFilename: '[name].bundle-[hash].css' }),
		new HtmlWebpackPlugin({ template: 'src/index.html', chunksSortMode: 'none' })
	],
	devtool: 'source-map'
};

function withSourceMap(loader) {
	return { loader, options: { sourceMap: true } };
}

function projectPaths(...rootPaths) {
	const toAbsolute = rootRelative => path.resolve(__dirname, '../', rootRelative);
	return rootPaths.map(toAbsolute);
}
