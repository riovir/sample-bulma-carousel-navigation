import SomeText from 'src/components/some-text';

export const WELCOME = TextContent({
	name: 'welcome',
	title: 'Hey there!',
	content: 'Welcome to this carousel demo. Use the arrows on the side to navigate.'
});

export const ARROWS = TextContent({
	name: 'arrows',
	title: 'Jump to Next or go Back',
	content: 'Use the arrows on the sides to jump around!'
});

export const DOTS = TextContent({
	name: 'dots',
	title: 'Or show item immediately',
	content: 'You can also click on the dots to jump straight to a slide.'
});

export const ORDERED = [
	WELCOME,
	ARROWS,
	DOTS
];

function TextContent({
	name,
	path = name,
	title = 'Missing title',
	content = 'No content'
}) {
	return {
		name,
		path,
		component: SomeText,
		props: { title, content }
	};
}
