import './style/app.scss';

import Vue from 'vue';
import * as vueSetup from './vue-setup';

new Vue({ el: '#app', render: h => h('router-view'), ...vueSetup });

/* eslint-disable-next-line no-console */
console.log("Page loaded. You won't see this message repeating when using HMR and editing vue files.");
