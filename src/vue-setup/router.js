import Vue from 'vue';
import VueRouter from 'vue-router';
import TheApp from 'src/the-app';
import { ORDERED } from 'src/content';

Vue.use(VueRouter);

const routes = [
	{ path: '/', redirect: ORDERED[0].name },
	{ path: '/', component: TheApp, props: { routes: ORDERED }, children: ORDERED }
];

export const router = new VueRouter({
	routes,
	mode: 'history',
	base: process.env.PUBLIC_PATH
});
