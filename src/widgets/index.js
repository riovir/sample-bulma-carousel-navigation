import BulmaCarousel from './bulma-carousel';

export default function install(Vue) {
	Vue.component(BulmaCarousel.name, BulmaCarousel);
}
