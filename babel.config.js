module.exports = {
	presets: [
		['@babel/preset-env', {
			corejs: 2,
			modules: false,
			useBuiltIns: 'usage'
		}]
	],
	plugins: [
		['@babel/plugin-syntax-dynamic-import'],
	],
	env: {
		test: {
			presets: [
				['@babel/preset-env', { targets: { node: 'current' } }]
			],
			plugins: [
				['transform-dynamic-import'],
			]
		},
	}
};
